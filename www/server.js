var express = require('express');
var app = express();

var config = {
	port : {
		insecure : 80,
		secure : 443
	}
}

app.use(express.static('public_html'));

app.get(['/', '/*'], function(req, res) {
	res.sendFile(__dirname + '/public_html/index.html');
});


app.listen(config.port.insecure, function() {
	console.log('unicef listening on ', config.port.insecure);
})
