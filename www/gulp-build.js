var gulp = require('gulp');
var sequence = require('run-sequence');

require('require-dir')('./gulp/tasks', { recurse: true });
gulp.task('default', function() {
	return sequence('build-all');
});
