/* * * * * * *BUILD PROCESS OVERVIEW  * * * * * */
//CSS: WRITE SCSS => COMPASS (VM) => PUBLIC_HTML/CSS => GULP CSS => PUBLIC_HTML/ASSETS => COMPILE INDEX.HBS
//SITE JS : WRITE SITE JS => GULP SITE => PUBLIC_HTML/ASSETS => COMPILE INDEX.HBS
//MODULES JS : ADD MODULES TO MODULE.JS => GULP MODULES => PUBLIC_HTML/ASSETS => COMPILE INDEX.HBS

/* * * * * * *COMPASS SETUP INSTRUCTIONS - WINDOWS * * * * * */
//INSTALL RUBY http://rubyinstaller.org/downloads/
//ADD RUBY/BIN PATH TO WINDOWS PATH
//GEM INSTALL COMPASS

var fs = require('fs');
var argv = require('yargs').argv;

var gulp = require('gulp');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var ngAnnotate = require('gulp-ng-annotate');
var sequence = require('run-sequence');

var gutil = require('gulp-util');
var plumber = require('gulp-plumber');
var gulpif = require('gulp-if');
var rev = require('gulp-rev');
var revDel = require('rev-del');

var compass = require('gulp-compass');
var minifyCSS = require('gulp-minify-css');

var handlebars = require('gulp-compile-handlebars');
var rename = require('gulp-rename');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');

var templateCache = require('gulp-angular-templatecache');
var htmlmin = require('gulp-htmlmin');

argv.env = argv.env || 'production'; //very important, default to prod if nothing is provided
argv.build = argv.build || 1;
argv.rev = argv.rev || 'a';

var errorHandler = function(error) {
	gutil.beep();
	console.log(error.toString());
}

//this is run in gulp-build.js when builds happen in bamboo
gulp.task('build-all', function() {
	return sequence('build-modules', 'build-templates', 'build-site', 'build-sass', 'build-css', 'compile');
});

/*
gulp.task('app', function() {
	return sequence('build-app', 'compile');
});
*/

gulp.task('site', function() {
	return sequence('build-templates', 'build-site', 'compile');
});

gulp.task('modules', function() {
	return sequence('build-modules', 'compile');
});

gulp.task('css', function() {
	return sequence('build-sass', 'build-css', 'compile');
});

gulp.task('build-modules', function() {
	return browserify(argv.env == 'production' ? './modules.js' : './modules.local.js')
		.bundle()
		.pipe(source('bundle.js'))
		.pipe(buffer())
		.pipe(gulpif((argv.env == 'production'), uglify().on('error', gutil.log)))
		.pipe(gulp.dest('build/assets/'))
		.pipe(rev())
		.pipe(gulp.dest('public_html/assets'))
		.pipe(rev.manifest({'merge' : true}))
		.pipe(revDel({'dest' : 'public_html/assets', 'oldManifest' : 'rev-manifest.json'}))
		.pipe(gulp.dest('.'));
});

gulp.task('build-css', function () {
	return gulp.src(['public_html/css/*.css'])
		.pipe(plumber(errorHandler))
		//.pipe(concat('style.css'))
		.pipe(gulp.dest('build/assets/'))
		.pipe(rev())
		.pipe(gulp.dest('public_html/assets'))
		.pipe(rev.manifest({'merge' : true}))
		.pipe(revDel({'dest' : 'public_html/assets', 'oldManifest' : 'rev-manifest.json'}))
		.pipe(gulp.dest('.'));
})

gulp.task('build-site', function () {
	var src = [];
	console.log('environment', argv.env);
	src.push('scripts/main.js');


	//use command line to determine which file to load
	var path = 'scripts/modules/module.httpconfig.'+(argv.env || 'production')+'.js';
	try {
		fs.accessSync(path, fs.R_OK)
		src.push(path);
	} catch(err) {
		console.log('failed to include ', path, err)
	}

	//src.push('scripts/modules/http');
	src.push('build/assets/templates.js');
	src.push('scripts/classes/**/*.js');
	src.push('scripts/controllers/**/*.js');
	src.push('scripts/directives/**/*.js');
	src.push('scripts/filters/**/*.js');

	return gulp.src(src)
		.pipe(plumber(errorHandler))
		.pipe(gulpif((argv.env != 'production'), sourcemaps.init()))
			.pipe(ngAnnotate().on('error', gutil.log))
			.pipe(gulpif((argv.env == 'production'), uglify().on('error', gutil.log)))
			.pipe(concat('site.js'))
		.pipe(gulpif((argv.env != 'production'), sourcemaps.write()))
		.pipe(gulp.dest('build/assets/'))
		.pipe(rev())
		.pipe(gulp.dest('public_html/assets'))
		.pipe(rev.manifest({'merge' : true }))
		.pipe(revDel({'dest' : 'public_html/assets', 'oldManifest' : 'rev-manifest.json'}))
		.pipe(gulp.dest('.'));
})

gulp.task('build-app', function () {
	return gulp.src(['build/assets/bundle.js', 'build/assets/site.js'])
		.pipe(plumber(errorHandler))
		.pipe(gulpif((!argv.env == 'production'), sourcemaps.init()))
			.pipe(ngAnnotate().on('error', gutil.log))
			.pipe(gulpif((argv.env == 'production'), uglify().on('error', gutil.log)))
			.pipe(concat('app.js'))
		.pipe(gulpif((argv.env != 'production'), sourcemaps.write()))
		.pipe(gulp.dest('build/assets/'))
		.pipe(rev())
		.pipe(gulp.dest('public_html/assets'))
		.pipe(rev.manifest({'merge' : true}))
		.pipe(revDel({'dest' : 'public_html/assets', 'oldManifest' : 'rev-manifest.json'}))
		.pipe(gulp.dest('.'));
})

gulp.task('build-sass', function() {
	return gulp.src(__dirname+'/../../sass/**/*.scss')
		.pipe(plumber(errorHandler))
		.pipe(compass({
			project: __dirname+'/../../sass/',
			config_file:  __dirname+'/../../sass/config.rb',
			debug: false,
			sass:  __dirname+'/../../sass/',
			css:  __dirname+'/../../public_html/css/',
			environment: (argv.env == 'production' ? 'production' : 'development')
		}).on('error', function(error) {
			console.log(error);
		}));
});


/**
	SASS/CSS SOURCE MAPS
	For some reason, it seems as though the revision manifesting breaks source maps between the .css and .scss source files.
	This this reason, we need to point development environments to the place compass puts the original css and css.map files.
**/


var handlebarOpts = {
    helpers: {
        stylePath: function (path, context) { //set this custom for source map purposes
            return [(argv.env == 'production' ? '/assets' : '/css'), context.data.root[path]].join('/');
        },
        assetPath: function (path, context) {
            return ['/assets', context.data.root[path]].join('/');
        }
    }
};

gulp.task('compile', function() {
	var manifest = JSON.parse(fs.readFileSync('rev-manifest.json', 'utf8'));
	if (argv.env != 'production')
		manifest['style.css'] = 'style.css';

    // read in our handlebars template, compile it using
    // our manifest, and output it to index.html
    return gulp.src('index.hbs')
        .pipe(handlebars(manifest, handlebarOpts))
        .pipe(rename('index.html'))
        .pipe(gulp.dest('public_html'));

    gutil.beep();
    return r;
})

//combined this into templates below
//gulp.task('htmlmin', function() {
//	return gulp.src('templates/**/*.html')
//		.pipe(htmlmin({ 
//			collapseWhitespace : true, 
//			removeComments : true 
//		}))
//		.pipe(gulp.dest('build/templates'));
//})


gulp.task('build-templates', function() {
	return gulp.src('templates/**/*.html')
		.pipe(plumber(errorHandler))
		.pipe(htmlmin({ 
			collapseWhitespace : true, 
			removeComments : true,
			ignoreCustomFragments: [ /\{\{[\s\S]*?\}\}/ ] //ignore content within handlebars
		}))
		.pipe(templateCache({
			module : 'app',
			root : '/templates/'
		}))
		.pipe(gulp.dest('build/assets'));
})