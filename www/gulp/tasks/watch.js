var gulp = require('gulp');
var watch = require('gulp-watch');
var argv = require('yargs').argv;

gulp.task('default', ['modules', 'site', 'css', 'build-templates'], function() {
    gulp.watch(['scripts/**/*.js'], { interval : 300 }, ['site']);
    gulp.watch(['templates/**/*.html'], { interval : 300 }, ['site']);
    gulp.watch(['sass/**/*.scss'], { interval : 300 }, ['css']);
    gulp.watch(['index.hbs'], { interval : 300 }, ['compile']);
    gulp.watch(['modules.js', 'node_modules/@cevo/**/*.js'], { interval : 300 }, ['modules']);

	if (argv.env == 'local')
    	gulp.watch(['modules.local.js', '../../angular-cte/index.js', '../../angular-util/index.js'], { interval : 300 }, ['modules']);
});