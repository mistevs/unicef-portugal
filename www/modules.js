require('angular');
require('angular-route');
require('angular-sanitize');
require('angular-animate');
require('angular-resource');
require('@uirouter/angularjs');
require('ngstorage');
require('angular-moment');
require('angular-timer');

window.moment = require('moment'); //this hacky shit is required for angular-timer to work
window.humanizeDuration = require('humanize-duration'); //this hacky shit is required for angular-timer to work

require('angulartics');
require('angulartics-google-analytics');

require('@cevo/angular-util');