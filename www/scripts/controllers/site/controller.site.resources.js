angular.module('app').controller('SiteResourcesController', function($scope, Logger, Resource) {
	var ctrl = this;

	$scope.filters = {
		'types' : {
			'pdf' : true,
			'video' : true
		},
		'ages' : {
			'4' : true,
			'6' : true,
			'10' : true,
			'14' : true
		}
	}

	ctrl.resources = [];
	var old = [{
		'title' : 'Co-Operation Games',
		'age' : 4,
		'type' : 'PDF'
	},
	{
		'title' : 'Photo Gallery',
		'age' : 4,
		'type' : 'PDF'
	},
	{
		'title' : 'Convention on the Rights of the Child',
		'age' : 4,
		'type' : 'Video'
	}];

	ctrl.themes = [];
	var old = [{
		'title' : 'Armed Conflict',
		'age' : 4,
		'type' : 'PDF'
	},
	{
		'title' : 'Child Labor',
		'age' : 4,
		'type' : 'PDF'
	},
	{
		'title' : 'Child’s Rights (CRC)',
		'age' : 4,
		'type' : 'Video'
	},
	{
		'title' : 'Child Trafficking',
		'age' : 4,
		'type' : 'Video'
	},
	{
		'title' : 'Children with Disabilities',
		'age' : 4,
		'type' : 'PDF'
	},
	{
		'title' : 'Education',
		'age' : 14,
		'type' : 'Video'
	},
	{
		'title' : 'Emergencies',
		'age' : 14,
		'type' : 'PDF'
	},
	{
		'title' : 'Gender Equality',
		'age' : 14,
		'type' : 'PDF'
	},
	{
		'title' : 'Global Citizenship',
		'age' : 6,
		'type' : 'Video'
	},
	{
		'title' : 'Health',
		'age' : 6,
		'type' : 'PDF'
	},
	{
		'title' : 'HIV/AIDS',
		'age' : 10,
		'type' : 'Video'
	},
	{
		'title' : 'Nutrition',
		'age' : 10,
		'type' : 'PDF'
	},
	{
		'title' : 'Peace Education',
		'age' : 10,
		'type' : 'PDF'
	},
	{
		'title' : 'Poverty',
		'age' : 10,
		'type' : 'PDF'
	},
	{
		'title' : 'Sustainable Development Goals',
		'age' : 10,
		'type' : 'PDF'
	},
	{
		'title' : 'Water and Environment',
		'age' : 10,
		'type' : 'PDF'
	}];

	for (var i in ctrl.themes)
		ctrl.themes[i].tag = ctrl.themes[i].title.replace(/ /g, '-');

	ctrl.filteredResources = ctrl.resources;
	ctrl.filteredThemes = ctrl.themes;

	$scope.$on('$destroy', function() {
		
	})

	$scope.$watch('filters', function() {
		ctrl.filterResources();
	}, true);

	ctrl.Load = function() {
		ctrl.LoadResources();
		Logger.debug('loaded resources controller');
	}

	ctrl.LoadResources = function() {
		(new Resource).$list().then(function(resources) {
			var themes = [];
			var r = [];

			for (var i in resources)
			{
				if (resources[i].type == 'theme')
					themes.push(resources[i]);
				else if (resources[i].type == 'resource')
					r.push(resources[i]);
			}

			Logger.debug('loaded resources', resources);

			ctrl.themes = themes;
			ctrl.resources = r;

			ctrl.filterResources();
		});
	}

	ctrl.filterResources = function() {
		var filteredResources = [];
		var filteredThemes = [];

		for (var i in ctrl.resources) {
			var resource = ctrl.resources[i];

			//if ($scope.filters.ages[resource.lesson.minAge] && $scope.filters.types[resource.type.toLowerCase()])
			filteredResources.push(resource);
		}

		Logger.debug('filtering themes', ctrl.themes);

		for (var i in ctrl.themes) {
			var theme = ctrl.themes[i];

			Logger.debug('checking theme', theme.lesson);
			if ($scope.filters.ages[theme.lesson.minAge])
				filteredThemes.push(theme);
		}

		Logger.debug('Filtered themes', filteredThemes);

		ctrl.filteredResources = filteredResources;
		ctrl.filteredThemes = filteredThemes;
	}

	ctrl.Load();
	
	return ctrl;
});