angular.module('app').controller('SiteDashboardController', function($scope, $state, User, Logger, cevo) {
	var ctrl = this;

	ctrl.selectedLesson = false;

	$scope.$on('$destroy', function() {
		
	})

	User.$on(['login', 'verify'], function() {
		ctrl.LoadActivities();
	})

	User.$on(['logout', 'verifyFail'], function() {
		$state.go('site.home');
	})

	ctrl.Load = function() {
		Logger.debug('loaded dashboard controller');
		
		if (User.token)
			ctrl.LoadActivities();
	}

	ctrl.LoadActivities = function() {
		User.$activities().then(function(activities) {
			ctrl.activities = activities;
			Logger.debug('activities', ctrl.activities);
		})
	}

	ctrl.SaveRating = function(activity, rating) {
		activity.lessonrating.rating = rating;
		activity.rating = 0;
		if (activity.lessonrating.id)
			activity.lessonrating.$put().then(function() { activity.lessonrating.$get(); });
		else
		{
			activity.lessonrating.lesson = activity.id;
			activity.lessonrating.$post().then(function() { activity.lessonrating.$get(); });
			Logger.debug('posting lessonrating', activity.lessonrating);
		}
	}

	ctrl.ShowFeedback = function(activity) {
		ctrl.activity = activity;
		ctrl.lessonrating = activity.lessonrating;
		ctrl.feedback = activity.lessonrating.comment;
	}

	ctrl.SaveFeedback = function() {
		var callback = function() {
			ctrl.activity = false;
			ctrl.lessonrating = false;
			ctrl.feedback = false;
		};

		ctrl.lessonrating.comment = ctrl.feedback;
		if (ctrl.lessonrating.id)
			ctrl.lessonrating.$put().then(callback);
		else
		{
			ctrl.lessonrating.lesson = ctrl.activity.id;
			ctrl.lessonrating.$post().then(callback);
		}
	}

	ctrl.CloseFeedback = function(activity) {
		ctrl.feedback = false;
	}

	ctrl.EditActivity = function() {
		Logger.debug('Editing Activity...');
	}

	var oldProfile = {}
	ctrl.EditProfile = function() {
		oldProfile = cevo.util.getAttributes(User);
		ctrl.editingProfile = true;
	}

	ctrl.CancelProfile = function() {
		ctrl.editingProfile = false;
		for (i in oldProfile)
			User[i] = oldProfile[i];
	};

	ctrl.SaveProfile = function() {
		ctrl.saving = true;
		ctrl.$promise = User.$put().then(function() {
			User.$get();
			ctrl.editingProfile = false;
		}).finally(function() {
			ctrl.saving = false;
		});
	}

	ctrl.Load();
	
	return ctrl;
});