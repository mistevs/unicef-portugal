angular.module('app').controller('SiteController', function($scope, $rootScope, Logger, Framework) {
	Logger.debug('loaded site controller');

	var site = this;
	
	site.framework = Framework;

	site.state = {
		modalActive : false,
		login : 0,
		registration  : 0
	}

	$rootScope.Object = {
		size : function(obj) {
			var size = 0, key;

			for (key in obj) {
				if (obj.hasOwnProperty(key)) 
					size++;
			}

			return size;
		}
	}

	$rootScope.$on('modal:active', function() {
		site.state.modalActive = true;
	})

	$rootScope.$on('login:success', function() {
		site.state.login=0;
		site.state.registration=0;
		site.state.modalActive=false;
	})

	$rootScope.$on('modal:inactive', function() {
		site.state.modalActive = false;
	})

	site.Load = function() {
	}

	site.Register = function(redirectUrl) {
		$rootScope._login.hide();
		//site.state.registration = 0;
		//site.state.login = 0;
		Logger.debug('setting registration state', site.state.login, site.state.registration);
		if (redirectUrl)
			$rootScope.redirectUrl = redirectUrl;
	}

	site.Login = function(redirectUrl) {
		$rootScope._login.show();
		//site.state.registration = 0;
		//site.state.login = 1;
		Logger.debug('setting login state', site.state.login, site.state.registration);
		if (redirectUrl)
			$rootScope.redirectUrl = redirectUrl;
	}

	site.Load();
	
	return site;
});