angular.module('app').controller('SiteHomeController', function($state, $scope, $rootScope, $log, $filter, $http, $location, $window, $interval, $timeout, $localStorage, Logger) {
	home = this;

	$scope.$on('$destroy', function() {
		
	})

	home.Load = function() {
		Logger.debug('loaded home controller');
	}

	home.Load();
	
	return home;
});