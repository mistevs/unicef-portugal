angular.module('app').controller('SiteResourceController', function($stateParams, $scope, $rootScope, Logger, Resource, User, $window) {
	var ctrl = this;

	ctrl.resource = new Resource($stateParams.resource);

	$scope.$on('$destroy', function() {
		
	})

	$scope.$watch('filters', function() {
		ctrl.filterLessons();
	}, true);

	$scope.filters = {
		'types' : {
			'pdf' : true,
			'video' : true
		},
		'ages' : {
			'4' : true,
			'6' : true,
			'10' : true,
			'14' : true
		}
	}

	var old = [{
		'title' : 'Where in the world?',
		'minAge' : 4,
		'maxAge' : 5,
		'type' : 'PDF'
	},
	{
		'minAge' : 6,
		'maxAge' : 9,
		'type' : 'PDF'
	},
	{
		'minAge' : 10,
		'maxAge' : 13,
		'type' : 'video'
	},
	{
		'minAge' : 14,
		'maxAge' : 16,
		'type' : 'video'
	},
	{
		'minAge' : 14,
		'maxAge' : 16,
		'type' : 'video'
	},
	{
		'minAge' : 14,
		'maxAge' : 16,
		'type' : 'video'
	},
	{
		'minAge' : 14,
		'maxAge' : 16,
		'type' : 'video'
	}];

	ctrl.Load = function() {
		Logger.debug('loaded resource controller', ctrl.resource);
		ctrl.resource.$get();
		ctrl.resource.$lessons().then(function() {
			ctrl.filterLessons();
		});

	}

	ctrl.selectedLesson = {};
	ctrl.selectLesson = function(lesson) {
		if (!User.token)
			return $rootScope._login.show();

		ctrl.selectedLesson = lesson;
		Logger.debug('Selecting lesson', lesson);
	}

	ctrl.filteredLessons = ctrl.resource.lessons;
	ctrl.filterLessons = function() {

		var filteredLessons = [];

		for (var i in ctrl.resource.lessons) {
			var lesson = ctrl.resource.lessons[i];

			if ($scope.filters.ages[lesson.minAge] && $scope.filters.types[lesson.type.toLowerCase()])
				filteredLessons.push(lesson);
		}

		ctrl.filteredLessons = filteredLessons;
		Logger.debug('Filtering lessons ...', ctrl.filteredLessons, ctrl.resource.lessons);
	}

	ctrl.Load();
	
	return ctrl;
});