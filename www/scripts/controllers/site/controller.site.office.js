angular.module('app').controller('SiteOfficeController', function($scope, $state, $q, cevo, Logger, Resource, Lesson, User, Notification) {
	var ctrl = this;

	$scope.form = {
		resource : new Resource(),
		lesson : new Lesson()
	}

	ctrl.$promise = {
		resource : null,
		lesson : null
	}

	$scope.$on('$destroy', function() {
			
	});

	User.$on(['login', 'verify'], function() {
		ctrl.Load();
	});

	User.$on(['logout', 'verifyFail'], function() {
		$state.go('site.home');
	})

	ctrl.Load = function() {
		if (!User.token)
			return;
		
		Logger.debug('loaded office controller');

		ctrl.Authorize().then(function() {
			ctrl.LoadResources();
		}, function() {
			$state.go('site.home');
		});
	}

	ctrl.Authorize = function() {
		return User.$authorize();
	}

	ctrl.LoadResources = function() {
		 (new Resource).$list().then(function(resources) {
		 	ctrl.resources = resources;
			Logger.debug('Loaded resources', ctrl.resources);
		});
	}

	ctrl.SetResource = function(resource) {
		Logger.debug(resource);
		ctrl.resource = resource;
		$scope.form.resource = resource;
		
		ctrl.resource.$lessons().then(function(lessons) {
			Logger.debug('loaded lesspons', lessons);
		});
	}

	ctrl.AddResource = function() {
		$scope.form.resource = new Resource();
		ctrl.resource = $scope.form.resource;
		ctrl.resource.editing = true;
	}

	ctrl.EditResource = function(resource) {
		$scope.form.resource = resource;
		ctrl.resource = resource;
		ctrl.resource.editing = true;
	}

	ctrl.SaveResource = function() {
		var callback = function() {
			ctrl.LoadResources();
			$scope.form.resource = new Resource();
			ctrl.resource.editing = false;
		};

		if ($scope.form.resource.id)
			ctrl.$promise.resource = $scope.form.resource.$put().then(callback);
		else
			ctrl.$promise.resource = $scope.form.resource.$post().then(callback);
	}

	ctrl.DeleteResource = function(resource) {
		resource.$delete().then(function() {
			ctrl.LoadResources();
			Notification.success('The resource has successfully been removed');
		});
	}

	ctrl.AddLesson = function() {
		$scope.form.lesson = new Lesson();
		ctrl.lesson = $scope.form.lesson;
		ctrl.lesson.editing = true;
	}

	ctrl.EditLesson = function(lesson) {
		lesson.editing = true;
		$scope.form.lesson = lesson;
		ctrl.lesson = lesson;
	}

	ctrl.SaveLesson = function() {
		var callback = function() {
			ctrl.lesson.editing = false;
			ctrl.LoadResources();
			ctrl.SetResource(ctrl.resource);
			$scope.form.lesson = new Lesson();
		};

		Logger.debug('Saving lesson', $scope.form.lesson, ctrl.lesson);
		if ($scope.form.lesson.id)
			ctrl.$promise.lesson = $scope.form.lesson.$put().then(callback);
		else
		{
			$scope.form.lesson.resource = ctrl.resource.id;
			ctrl.$promise.lesson = $scope.form.lesson.$post().then(callback);
		}
	}

	ctrl.DeleteLesson = function(lesson) {
		lesson.$delete().then(function() {
			ctrl.SetResource(ctrl.resource);
			ctrl.LoadResources();
			Notification.success('The lesson has successfully been removed');
		});
	}

	ctrl.Load();
	
	return ctrl;
});