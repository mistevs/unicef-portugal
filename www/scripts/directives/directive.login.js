angular.module('app').directive('login', function(cevo, Logger, $rootScope, User, $localStorage, Notification) {
	return {
		restrict: 'E',
		templateUrl : '/templates/directives/directive.login.html',
		transclude: true,
		scope : {},
		link : function(scope, elem, attrs) {
			$rootScope.User = User;

			scope.force = attrs.force === 'true';
			scope.redirect = attrs.redirect === 'true';
			scope.modal = {
				show : false
			}

			
			//expose a rootscope object to show and hide the modal
			$rootScope._login = {
				show : function() {
					showModal();
				},

				hide : function() {
					hideModal();
				}
			}

			var showModal = function() {
				scope.modal.show = true;
				resetForm();
			}

			var hideModal = function() {
				scope.modal.show = false;
			}

			var resetForm = function() {
				//$log.log('resetting form');
				scope.form = {
					email : '',
					password : '',
					pane : 'login',
					remember : true,
					processing : false
				};
			};

			scope.showRegistration = function() {
				hideModal();
				$rootScope._registration.show();
			}

			if (scope.force)
			{	
				User.$on(['verifyFail', 'loginFail'], function() {
					showModal();
				})
			}

			User.$on(['login'], function() {
				hideModal();
				resetForm();
			});

			User.$on(['login', 'verify'], function() {
				User.$get().then(function() {
					Logger.debug('User', User);
					//if (!User.verified)
					//	$rootScope._registration.show();
				});
			})

			User.$verify();

			scope.Login = function() {
				if (!scope.form.email || !scope.form.password)
					return;

				Logger.debug('Logging in', scope.form);
				scope.form.processing = true;
				scope.$promise = User.$authenticate(scope.form).finally(function() {
					scope.form.processing = false;
				});
			};

			scope.ResetPassword = function(email) {
				scope.form.processing = true;
				User.$resetPassword(email).then(function(user) {
					Notification.success('Reset password email has been sent');
				}).finally(function() {
					scope.form.processing = false;
				});
			};

			resetForm();
		}
	};
});