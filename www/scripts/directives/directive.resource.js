angular.module('app').directive('resource', function(Logger, $window) {
	return {
		restrict: 'E',
		templateUrl : '/templates/directives/directive.resource.html',
		replace: true,
		scope : {
			'lesson' : '='
		},
		link : function(scope, elem, attr) {
			scope.$watch('lesson', function() {
				Logger.debug('Heard lesson change', scope.lesson);
			}, true);

			scope.closeLesson = function() {
				Logger.debug('Closing lesson', scope.lesson);
				scope.lesson = false;
			}

			scope.downloadLesson = function() {
				scope.lesson.$download().then(function() {
					scope.lesson = false;
				});
				
				if (scope.lesson.type == 'pdf')	
					$window.open(scope.lesson.url, '_blank');
			}
		}
	};
});