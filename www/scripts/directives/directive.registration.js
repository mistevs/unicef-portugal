angular.module('app').directive('registration', function($rootScope, User, Settings, Logger, Notification) {
	return {
		restrict: 'E',
		templateUrl: '/templates/directives/directive.registration.html',
		transclude: true,
		scope : {},
		link: function(scope, element, attrs, $log) {
			scope.modal = {
				show : false
			}

			scope.form = {};

			scope.User = $rootScope.User;

			Logger.debug('linking registration ...');
			//expose a rootscope object to show and hide the modal
			$rootScope._registration = {
				show : function() {
					showModal();
				},

				hide : function() {
					hideModal();
				}
			}

			$rootScope.$watch('User.verified', function(val1, val2) {
				Logger.debug('Heard verified change', val1, val2, User.verified);

				if (User.verified && scope.settings.sentVerification)
				{
					scope.settings.step = 3;
					Notification.success('Your account has been successfully verified!');
				}
			})

			var showModal = function() {
				scope.Load();
				scope.modal.show = true;
			}

			var hideModal = function() {
				scope.modal.show = false;
			}

			var resetForm = function() {
				Logger.debug('Resetting registration form');
				scope.form.processing = false;
				scope.settings = {
					step : 1,
					sentVerification : false
				}

				scope.User.$resetAttr();
			}

			resetForm();

			scope.Load = function() {
				Logger.debug('Loading registration directive');
				resetForm();
			}

			scope.showLogin = function() {
				hideModal();
				$rootScope._login.show();
			}

			scope.Register = function() {
				scope.form.processing = true;

				Logger.debug('Registering user with form', scope.form);
				scope.$promise = User.$register(scope.form).then(function(data) {
					Logger.debug('Heard registration success', User);
					hideModal();
				}).finally(function() {
					scope.form.processing = false;
				});
			}

			scope.SendVerificationEmail = function(silenceNotify) {
				//scope.form.processing = true;
				return User.$verificationEmail().then(function() {
					scope.settings.sentVerification = true;
					if (!silenceNotify)
						Notification.success('An account verification email has been sent to '+User.email+'. Please follow the link in the email to complete your account registration.');
					
					Logger.debug('Verification email sent ...');
				}).finally(function() {
					scope.form.processing = false;
				});
			}

			scope.Load();
			
			return scope;
		}
	}
});