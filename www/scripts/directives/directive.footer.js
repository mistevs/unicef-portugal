angular.module('app').directive('cFooter', function() {
	return {
		restrict: 'E',
		templateUrl : '/templates/directives/directive.footer.html',
		replace: true
	};
});