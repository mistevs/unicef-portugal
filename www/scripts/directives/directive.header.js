angular.module('app').directive('cHeader', function() {
	return {
		restrict: 'E',
		templateUrl : '/templates/directives/directive.header.html',
		replace: true
	};
});