angular.module('app').factory('HTTPInterceptor', function ($q, $rootScope, Notification, Settings, Logger, Form) {
	return {
		'request': function (config) {
			if (Settings.token)
				config.headers['Authorization'] = 'Bearer '+Settings.token;

			if (config.method == 'DELETE')
				config.headers['Content-Type'] = 'application/json';

			return config || $q.when(config);
		},
		'responseError': function (data) {
			Logger.debug('Heard HTTP Response Error', data);
			if (data.status == 400 && data.data && data.data.errors)
			{
				Logger.debug('Saw errors, notifying new errors', data.data.errors);

				if (data.data.errors.global && data.data.errors.global.length > 0)
					Notification.error(data.data.errors.global);
			}

			return $q.reject(data);
		}
	};
})