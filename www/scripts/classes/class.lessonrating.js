angular.module('app').factory('LessonRating', function($http, cevo, Logger) {
	function Lesson(id) {
		var obj = {
			id : id,

			$get : function(id) {
				if (id)
					obj.id = id;

				return $http({
					method: 'GET',
					url: cevo.endpoint+'/1.0/lessonrating/',
					params : cevo.util.getAttributes(obj)
				}).then(function(data) {
					if (data)
						angular.extend(obj, data.data.data.lessonrating);
				});
			},

			$post : function() {
				Logger.debug('Posting new lesson rating', cevo.util.getAttributes(obj), obj);

				return $http({
					method: 'POST',
					url: cevo.endpoint+'/1.0/lessonrating/',
					data : cevo.util.getAttributes(obj)
				});
			},

			$put : function() {
				return $http({
					method: 'PUT',
					url: cevo.endpoint+'/1.0/lessonrating/',
					data : cevo.util.getAttributes(obj)
				});
			},

			$delete : function() {
				return $http({
					method: 'DELETE',
					url: cevo.endpoint+'/1.0/lessonrating/',
					params : {'id' : obj.id }
				});
			}
		}

		if (typeof(id) === 'object')
		{
			angular.extend(obj, id);
			obj.id = (typeof(obj.id) === 'object' ? 0 : obj.id);
		}

		return obj;
	}

	return Lesson;
});