angular.module('app').factory('Resource', function($http, Logger, cevo, Lesson) {
	function Resource(id) {
		var obj = {
			id : id,

			$get : function(id) {
				if (id)
					obj.id = id;

				return $http({
					method: 'GET',
					url: cevo.endpoint+'/1.0/resource/',
					params : cevo.util.getAttributes(obj)
				}).then(function(data) {
					if (data)
						angular.extend(obj, data.data.data.resource);
				});
			},

			$post : function() {
				return $http({
					method: 'POST',
					url: cevo.endpoint+'/1.0/resource/',
					data : cevo.util.getAttributes(obj)
				});
			},

			$put : function() {
				return $http({
					method: 'PUT',
					url: cevo.endpoint+'/1.0/resource/',
					data : cevo.util.getAttributes(obj)
				});
			},

			$lessons : function() {
				return $http({
					method: 'GET',
					url: cevo.endpoint+'/1.0/resource/lessons/',
					params : { id : obj.id }
				}).then(function(data) {
					obj.lessons = cevo.util.objectify(data.data.data.lessons, Lesson);
					return obj.lessons;
				});	
			},

			$list : function() {
				return $http({
					method: 'GET',
					url: cevo.endpoint+'/1.0/resource/list/',
					params : { id : obj.id }
				}).then(function(data) {
					return cevo.util.objectify(data.data.data.resources, Resource);
				});	
			},

			$delete : function() {
				return $http({
					method: 'DELETE',
					url: cevo.endpoint+'/1.0/resource/',
					params : {'id' : obj.id}
				});
			}
		}

		if (typeof(id) === 'object')
		{
			angular.extend(obj, id);
				obj.id = (typeof(obj.id) === 'object' ? 0 : obj.id);
		}

		return obj;
	}

	return Resource;
});