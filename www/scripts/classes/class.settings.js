angular.module('app').factory('Settings', function(Logger, Listener) {
	var obj = this;

	angular.extend(this, Listener);
	this.$registerListener('config');

	this.config = function(config) {
		obj = angular.extend(obj, config);

		this.$notify('config');
	}

	this.set = function(key, val) {
		obj[key] = val;
	}

	return this;
})