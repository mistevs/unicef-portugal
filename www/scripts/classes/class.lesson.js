angular.module('app').factory('Lesson', function($http, cevo, Logger) {
	function Lesson(id) {
		var obj = {
			id : id,

			$get : function(id) {
				if (id)
					obj.id = id;

				return $http({
					method: 'GET',
					url: cevo.endpoint+'/1.0/lesson/',
					params : cevo.util.getAttributes(obj)
				}).then(function(data) {
					if (data)
						angular.extend(obj, data.data.data.lesson);
				});
			},

			$post : function() {
				Logger.debug('Posting new lesson', cevo.util.getAttributes(obj), obj);

				return $http({
					method: 'POST',
					url: cevo.endpoint+'/1.0/lesson/',
					data : cevo.util.getAttributes(obj)
				});
			},

			$put : function() {
				return $http({
					method: 'PUT',
					url: cevo.endpoint+'/1.0/lesson/',
					data : cevo.util.getAttributes(obj)
				});
			},

			$delete : function() {
				return $http({
					method: 'DELETE',
					url: cevo.endpoint+'/1.0/lesson/',
					params : {'id' : obj.id }
				});
			},

			$download : function() {
				return $http({
					method: 'GET',
					url: cevo.endpoint+'/1.0/lesson/download/',
					params : {'id' : obj.id}
				});
			}
		}

		if (typeof(id) === 'object')
		{
			angular.extend(obj, id);
				obj.id = (typeof(obj.id) === 'object' ? 0 : obj.id);
		}

		return obj;
	}

	return Lesson;
});