angular.module('app').service('User', function($http, $q, $localStorage, cevo, Settings, Logger, Form, Listener, LessonRating) {
	var obj = this;
	
	this.id = 0;

	angular.extend(this, Listener);
	this.$registerListener('login');
	this.$registerListener('loginFail');
	this.$registerListener('logout');
	this.$registerListener('verify');
	this.$registerListener('verifyFail');
	this.$registerListener('register');
	this.$registerListener('registerFail');

	this.$get = function(id) {
		if (id)
			this.id = id;

		return $http({
			method: 'GET',
			url: cevo.endpoint+'/1.0/user/',
			params : { id : id }
		}).then(function(data) {
			if (data)
				angular.extend(obj, data.data.data.user);
		});
	};

	this.$put = function() {
		return $http({
			method: 'PUT',
			url: cevo.endpoint+'/1.0/user/',
			data : cevo.util.getAttributes(obj)
		});
	};

	this.$authenticate = function(form) {
		Logger.debug('Authenticating', form);
		return $http({
			method: 'POST',
			url: cevo.endpoint+'/1.0/user/login',
			data : form
		}).then(function(data) {
			var user = data.data.data.user
			angular.extend(obj, user);

			obj.updateToken(user.authenticationToken);

			Logger.debug('user', 'Authentication success', obj);
			
			if (form.remember)
			{
				$localStorage.user = { 
					token : user.authenticationToken, 
					id : user.id
				};
			}

			obj.$notify('login');
		}, function(data) {
			obj.$notify('loginFail');
			Logger.debug('Heard user authentication error', data);
			return $q.reject(data);
		});
	};

	this.$verify = function() {
		if (!$localStorage.user || !$localStorage.user.token || !$localStorage.user.id)
		{
			obj.$notify('verifyFail');
			return $q.reject();
		}

		return $http({
			method: 'POST',
			url: cevo.endpoint+'/1.0/user/verify',
			data : {
				token : $localStorage.user.token,
				id : $localStorage.user.id
			}
		}).then(function(data) {
			var user = data.data.data.user;
			angular.extend(obj, user);

			obj.updateToken(user.authenticationToken);

			obj.$notify('verify');
		}, function() {
			obj.$notify('verifyFail');
			obj.updateToken(false);
			return $q.reject();
		});
	};

	this.$register = function(form) {
		return $http({
			method: 'POST',
			url: cevo.endpoint+'/1.0/user/register',
			data : form
		}).then(function(data) {
			var user = data.data.data.user;
			angular.extend(obj, user);

			obj.updateToken(user.authenticationToken);

			if (form.remember)
			{
				$localStorage.user = {
					token : obj.token,
					id : obj.id
				};
			}

			obj.$notify('register');
		}, function(data) {
			obj.$notify('registerFail');
			return $q.reject(data);
		});
	};

	this.$authorize = function() {
		return $http({
			method: 'GET',
			url: cevo.endpoint+'/1.0/user/authorization'
		});	
	};

	this.updateToken = function(token) {
		obj.token = token;
		Settings.set('token', obj.token);
		
		if ($localStorage.user)
			$localStorage.user.token = token;
	};

	this.$resetPassword = function(email) {
		return $http({
			method: 'PUT',
			url: cevo.endpoint+'/1.0/user/password/',
			params : {
				email : email
			}
		}).then(function(data) {
			return data.data;
		});
	};

	this.$logout = function() {
		obj.$reset();
		obj.$notify('logout');
	};

	this.$resetAttr = function() {
		var attrs = cevo.util.getAttributes(this);
		for (var i in attrs)
			this[i] = null;
	}

	this.$reset = function() {
		obj.$resetAttr();
		obj.updateToken(null);

		Logger.debug('deleting local storage user');
		delete $localStorage.user;
	};

	this.$downloads = function() {
		return $http({
			method: 'GET',
			url: cevo.endpoint+'/1.0/user/downloads'
		}).then(function(data) {
			return data.data.data.downloads;
		});	
	};

	this.$activities = function() {
		return $http({
			method: 'GET',
			url: cevo.endpoint+'/1.0/user/activities'
		}).then(function(data) {
			var activities = data.data.data.activities;
			for (var i in activities)
				activities[i].lessonrating = new LessonRating(activities[i].lessonrating);

			return activities;
		});	
	}
})