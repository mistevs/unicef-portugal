angular.module('app', ['ngAnimate', 'ui.router', 'ngResource', 'ngSanitize', 'ngStorage', 'angularMoment', 'app-env', 'angulartics', 'angulartics.google.analytics', 'cevo.util', 'timer']);

angular.module('app').run(function($rootScope, $document, $transitions, Framework, Logger, cevo, User) {
	Framework.title = 'UNICEF - Portugal';

	$rootScope.Framework = Framework;
	$rootScope.Math = Math;
	$rootScope.cevo = cevo;

	$transitions.onSuccess({}, function() {
		Logger.debug('Route change success, scrolling to top ...');
		$document[0].body.scrollTop = $document[0].documentElement.scrollTop = 0; //scroll to top on route changes
	});

	Logger.debug('Running App', cevo.env, cevo.endpoint);
});

angular.module('app').config(function($compileProvider, $locationProvider, $stateProvider, $urlRouterProvider, $httpProvider, $qProvider, ENV, API_HOST, cevoProvider) {
	if (ENV == 'production')
		$compileProvider.debugInfoEnabled(false);
	else
		console.debug('Running app in', ENV.toUpperCase(), 'environment');

	cevoProvider.setEnvironment(ENV);
	cevoProvider.setEndpoint(API_HOST);
	cevoProvider.notification.setDefaultTimeout(7500);

	$locationProvider.html5Mode({ 'enabled' : true, 'requireBase' : false, 'rewriteLinks' : true });
	$httpProvider.interceptors.push('HTTPInterceptor');
	$qProvider.errorOnUnhandledRejections(false);

	$urlRouterProvider.otherwise('/home');

	$stateProvider
		.state('site', {
			abstract : true,
			controller : 'SiteController',
			controllerAs: 'site',
			templateUrl : '/templates/site/site.default.html',
			url : ''
		})
		.state('site.home', { 
			controller : 'SiteHomeController',
			templateUrl : '/templates/site/site.home.html',
			url : '/home'
		})
		.state('site.about', { 
			controller : 'SiteAboutController',
			templateUrl : '/templates/site/site.about.html',
			url : '/about'
		})
		.state('site.resources', { 
			controller : 'SiteResourcesController',
			controllerAs : 'page',
			templateUrl : '/templates/site/site.resources.html',
			url : '/resources'
		})
		.state('site.resource', { 
			controller : 'SiteResourceController',
			controllerAs : 'page',
			templateUrl : '/templates/site/site.resource.html',
			url : '/resource/:resource'
		})
		.state('site.teachers', { 
			controller : 'SiteTeachersController',
			templateUrl : '/templates/site/site.teachers.html',
			url : '/teachers'
		})
		.state('site.dashboard', { 
			controller : 'SiteDashboardController',
			controllerAs : 'page',
			templateUrl : '/templates/site/site.dashboard.html',
			url : '/dashboard'
		})
		.state('site.office', { 
			controller : 'SiteOfficeController',
			controllerAs : 'office',
			templateUrl : '/templates/site/site.office.html',
			url : '/office'
		})
		.state('site.terms', { 
			templateUrl : '/templates/site/site.terms.html',
			url : '/terms'
		})
		.state('site.privacy', { 
			templateUrl : '/templates/site/site.privacy.html',
			url : '/privacy'
		})
		.state('site.contact', { 
			templateUrl : '/templates/site/site.contact.html',
			url : '/contact'
		});
});