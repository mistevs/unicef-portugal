<?php 

namespace Unicef\Objects;

use Qik\Database\{DBObject, DBQuery};
use Qik\Utility\{Utility};

class Resource extends DBObject
{
	public function GetLessons()
	{
		return $this->Select('lesson.*')->join('lesson ON lesson.resource_id = resource.id')->where('resource.id', $this->id);
	}
}