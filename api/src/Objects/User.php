<?php 

namespace Unicef\Objects;

use Qik\Database\{DBObject, DBQuery};
use Qik\Utility\{Utility};

class User extends DBObject
{
	public function __construct($pk = null)
	{
		return parent::__construct($pk);
	}

	function GenerateAuthenticationToken(bool $refresh = false)
	{
		if ($refresh || empty($this->authenticationToken))
		{
			$token = $this->id.md5(microtime().'$$@#2342342424$@$');
			$this->authenticationToken = $token;
			$this->Update();
		}

		return $this->authenticationToken;
	}

	function VerifyToken(int $id, string $token)
	{
		$user = $this->Select('id')->where('id', $id)->where('authenticationToken', $token)->asObject(get_class($this))->Fetch();
		if ($user)
		{
			$this->__construct($user->id);
			$this->lastLogin = time();
			$this->Update();

			return $this;
		}

		return false;
	}

	function VerifyCredentials(string $email, string $password)
	{
		$user = $this->Select(['id', 'password'])->where('email', $email)->asObject(get_class($this))->Fetch();
		if ($user && password_verify($password, $user->password))
		{
			$this->__construct($user->id);
			$this->lastLogin = time();
			$this->Update();

			return $this;
		}

		return false;
	}

	function GetDownloads()
	{
		return DBQuery::Build()->from('lessondownload')->select(['lesson.type', 'lesson.minAge', 'lesson.maxAge', 'lesson.resource.title', 'lessondownload.stamp', 'lesson.title', 'resource.title as resource_title'])->where('lessondownload.user_id', $this->id);
	}

	function GetActivities()
	{
		return DBQuery::Build()
			->from('lesson')
			->select([
					'lesson.id', 
					'lesson.type', 
					'lesson.minAge', 
					'lesson.maxAge', 
					'lesson.resource.title as resource_title', 
					'lesson.title', 
					'lessonrating:id as lessonrating_id',
					'lessonrating:rating',
					'lessonrating:comment',
					'MAX(lessondownload.stamp) as lessondownload_stamp', 
				])
			->where('lessondownload:user_id = ?', $this->id)
			->group(['lesson.id', 'lessonrating.id', 'lessonrating.rating', 'lessonrating.comment']);
	}
}