<?php 

namespace Unicef\Controllers;

use Unicef\Controllers\{CommonController};
use Unicef\Objects\{Resource, Lesson};

use Qik\Utility\{Utility, Validator};
use Qik\Database\{DBQuery, DBResult};
use Qik\Exceptions\{APIException, APIInternalException};

class ResourceController extends CommonController
{
	public function Init() 
	{
		$this->_object = new Resource();
		
		return parent::Init();
	}

	public function POST()
	{
		$this->RequireAuthorization();

		$title = $this->GetVariable('title');
		$type = $this->GetVariable('type');

		Validator::ValidateNotEmpty($title, 'A resource title must be entered.', 'title');
		Validator::ValidateNotEmpty($type, 'A resource type must be entered.', 'type');

		if (strtolower($type) != 'resource' && strtolower($type) != 'theme')
			throw new APIException('The resource must be of type resource or theme.');

		$this->_object->title = $title;
		$this->_object->type = $type;
		$this->_object->stamp = time();
		$this->_object->Insert();
	}

	public function PUT()
	{
		$this->RequireAuthorization();

		$title = trim($this->GetVariable('title'));
		$type = trim($this->GetVariable('type'));

		Validator::ValidateNotEmpty($this->_object->id, 'A resource must be provided to edit.', 'global');
		Validator::ValidateNotEmpty($title, 'A resource title must be entered.', 'title');
		Validator::ValidateNotEmpty($type, 'A resource type must be entered.', 'type');

		$this->_object->title = $title;
		$this->_object->type = $type;
		$this->_object->Update();
	}

	public function DELETE()
	{
		$this->RequireAuthorization();

		if (empty($this->_object->id))
			throw new APIException('You must provide a resource to delete');

		$this->_object->Delete();
	}

	public function ListGET()
	{
		$list = $this->_object->Select('resource.*')->leftJoin('lesson ON lesson.resource_id = resource.id')->select(['MIN(lesson.minAge) as minAge', 'MAX(lesson.maxAge) as maxAge'])->group('resource.id');
		$this->response->AddData('resources', DBResult::CreateObjects($list, [new Resource, new Lesson]));
	}

	public function LessonsGet()
	{	
		$lessons = $this->_object->GetLessons();
		$this->response->AddData('lessons', DBResult::CreateObjects($lessons, [new Lesson]));
	}
}
