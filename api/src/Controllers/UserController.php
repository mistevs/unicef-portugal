<?php 

namespace Unicef\Controllers;

use Unicef\Controllers\{CommonController};
use Unicef\Objects\{User, Lesson, LessonRating, LessonDownload, Resource};

use Qik\Utility\{Utility, Validator};
use Qik\Database\{DBQuery, DBResult};
use Qik\Exceptions\{APIException, APIInternalException};

class UserController extends CommonController
{
	public function Init() 
	{
		$this->_object = new User();
		
		return parent::Init();
	}

	public function GET()
	{
		$this->_object = $this->_user;

		parent::GET();
	}

	public function PUT()
	{
		$this->RequireAuthentication();

		$this->_user->name = $this->GetVariable('name');
		$this->_user->email = $this->GetVariable('email');
		$this->_user->school = $this->GetVariable('school');
		$this->_user->subject = $this->GetVariable('subject');

		Validator::ValidateNotEmpty($this->_user->name, 'Please enter a name', 'name');
		Validator::ValidateNotEmpty($this->_user->email, 'Please enter an email address', 'email');
		Validator::ValidateEmail($this->_user->email, true, 'Please enter a valid email address', 'email');

		if (!$this->_user->IsFieldUnique('email'))
			throw new APIException('The email address entered is already taken.', 'email');

		Validator::ValidateNotEmpty($this->_user->school, 'Please enter a school name', 'school');

		$this->_user->Update();

		$this->response->AddData($this->_user);
	}

	public function AuthorizationGET()
	{
		$this->RequireAuthorization();
	}

	public function RegisterPOST()
	{
		$email = $this->GetVariable('email');
		$password = $this->GetVariable('password');
		$passwordConfirm = $this->GetVariable('passwordconfirm');
		$school = $this->GetVariable('school');
		$terms = $this->GetVariable('terms');

		Validator::ValidateNotEmpty($email, 'Please enter an email address', 'email');
		Validator::ValidateEmail($email, true, 'Please enter a valid email address', 'email');

		if (!$this->_object->IsFieldUnique('email', $email))
			throw new APIException('The email address entered is already taken.', 'email');

		Validator::ValidateNotEmpty($password, 'Please enter a password', 'password');
		Validator::ValidateMatch($password, $passwordConfirm, 'The passwords entered must match', 'passwordconfirm');

		if (!$terms)
			throw new APIException('You must agree to our Terms & Conditions and Privacy Policy', 'terms');

		$password = password_hash($password, PASSWORD_DEFAULT);

		$this->_object->email = $email;
		$this->_object->password = $password;
		$this->_object->school = $school;
		$this->_object->lastLogin = time();
		$this->_object->stamp = time();
		$this->_object->Insert();

		$this->_object->GenerateAuthenticationToken();

		$this->response->AddData($this->_object);
	}

	public function VerifyPOST()
	{
		$token = $this->GetVariable('token');
		$id = $this->GetVariable('id');

		if (!$this->_object->VerifyToken($id, $token))
			throw new APIException('Unable to verify user.');

		$this->response->AddData($this->_object);
	}

	public function LoginPOST()
	{
		$email = $this->GetVariable('email');
		$password = $this->GetVariable('password');
		
		Validator::ValidateNotEmpty($email, 'Please enter an email address', 'email');
		Validator::ValidateEmail($email, true, 'Please enter a valid email address', 'email');
		Validator::ValidateNotEmpty($password, 'Please enter a password', 'password');

		if (!$this->_object->VerifyCredentials($email, $password))
			throw new APIException('Invalid login credentials.', 'password');
		
		$this->response->AddData($this->_object);
	}

	public function ActivitiesGET()
	{
		$this->RequireAuthentication();
		$activities = $this->_user->GetActivities();
		$this->response->AddData('activities', DBResult::CreateObjects($activities, [new Lesson, new LessonDownload, new LessonRating, new Resource]));
	}

	public function DownloadsGET()
	{
		$this->RequireAuthentication();

		$this->response->AddData('downloads', DBResult::CreateObjects($this->_user->GetDownloads(), [new LessonDownload, new Lesson, new Resource]));
	}
}