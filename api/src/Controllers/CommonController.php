<?php 

namespace Unicef\Controllers;

use Qik\Core\APIController;
use Qik\Utility\Utility;
use Qik\Exceptions\APIException;
use Qik\Database\DBManager;

use Unicef\Objects\{User};

class CommonController extends APIController
{
	public function Init() 
	{
		$pk = $this->GetVariable($this->_object->GetPrimaryKeyColumn());

		if (!empty($pk))
			$this->_object->__construct($pk);

		$this->_user = $this->GetAuthenticatedUser();

		return parent::Init();
	}

	public function GetVariable($key, $default = null)
	{
		return self::$server->GetVariable($key, $default);
	}

	public function GetAuthenticatedUser()
	{
		if (is_object($this->_user) && !empty($this->_user->id))
			return $this->_user;

		$header = self::$server->GetRequestHeaderData('Authorization');
		$authorization = explode(' ', $header);

		if (count($authorization) < 2 || $authorization[0] != 'Bearer')
			return new User();
		else
		{
			$user = (new User)->Select()->where('authenticationToken', $authorization[1])->asObject(get_class(new User))->Fetch();
			return $user;
		}
	}

	public function RequireAuthentication()
	{
		$this->response->DisableCache();

		if (!$user = $this->GetAuthenticatedUser())
			$this->response->SendUnauthorized();

		return $user;
	}

	public function RequireAuthorization()
	{
		$this->RequireAuthentication();

		if ($this->_user->type != 'staff')
			throw new APIException('You are not authorized to perform this action');
	}

	public function GET()
	{
		$this->response->AddData($this->_object);
	}

	public function PingGET()
	{
		$this->response->AddData('ping', 'back');
	}
}