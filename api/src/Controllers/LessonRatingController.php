<?php 

namespace Unicef\Controllers;

use Unicef\Controllers\{CommonController};
use Unicef\Objects\{Lesson, Resource, LessonRating, LessonDownload};

use Qik\Utility\{Utility, Validator};
use Qik\Database\{DBQuery, DBResult};
use Qik\Exceptions\{APIException, APIInternalException};

class LessonRatingController extends CommonController
{
	public function Init() 
	{
		$this->_object = new LessonRating();
		
		return parent::Init();
	}

	public function POST()
	{
		$this->RequireAuthentication();

		$comment = $this->GetVariable('comment');
		$rating = $this->GetVariable('rating');
		$lesson = $this->GetVariable('lesson');

		$this->_object->lesson_id = $lesson;
		$this->_object->user_id = $this->_user->id;
		$this->_object->rating = $rating;
		$this->_object->comment = $comment;
		$this->_object->stamp = time();
		$this->_object->Insert();
	}

	public function PUT()
	{
		$this->RequireAuthentication();

		$comment = $this->GetVariable('comment');
		$rating = $this->GetVariable('rating');

		$this->_object->rating = $rating;
		$this->_object->comment = $comment;
		$this->_object->Update();
	}
}
