<?php 

namespace Unicef\Controllers;

use Unicef\Controllers\{CommonController};
use Unicef\Objects\{Lesson, Resource, LessonDownload};

use Qik\Utility\{Utility, Validator};
use Qik\Database\{DBQuery, DBResult};
use Qik\Exceptions\{APIException, APIInternalException};

class LessonController extends CommonController
{
	public function Init() 
	{
		$this->_object = new Lesson();
		
		return parent::Init();
	}

	public function POST()
	{
		$this->RequireAuthorization();

		$title = $this->GetVariable('title');
		$type = $this->GetVariable('type');
		$url = $this->GetVariable('url');
		$minAge = $this->GetVariable('minAge');
		$maxAge = $this->GetVariable('maxAge');
		$resource = $this->GetVariable('resource');

		Validator::ValidateNotEmpty($title, 'A lesson title must be entered.', 'title');
		Validator::ValidateNotEmpty($url, 'A lesson URL must be entered.', 'url');
		
		Validator::ValidateNotEmpty($type, 'A lesson type must be entered.', 'type');
		if (strtolower($type) != 'pdf' && strtolower($type) != 'video')
			throw new APIException('The lesson type must be pdf or video.');

		Validator::ValidateNotEmpty($minAge, 'A minimum age must be entered.', 'minAge');
		Validator::ValidateNotEmpty($maxAge, 'A maximum age must be entered.', 'maxAge');
		if ($maxAge < $minAge)
			throw new APIException('The maximum age must be greater than the minimum age', 'maxAge');

		$resource = new Resource($resource);
		Validator::ValidateNotEmpty($resource->id, 'An invalid resource was given.', 'global');



		$this->_object->resource_id = $resource->id;
		$this->_object->title = $title;
		$this->_object->type = $type;
		$this->_object->url = $url;
		$this->_object->minAge = $minAge;
		$this->_object->maxAge = $maxAge;
		$this->_object->updated = time();
		$this->_object->stamp = time();
		$this->_object->Insert();
	}

	public function DELETE()
	{
		$this->RequireAuthorization();

		if (empty($this->_object->id))
			throw new APIException('You must provide a lesson to delete');

		$this->_object->Delete();
	}

	public function PUT()
	{
		$this->RequireAuthorization();

		$title = $this->GetVariable('title');
		$type = $this->GetVariable('type');
		$url = $this->GetVariable('url');
		$minAge = $this->GetVariable('minAge');
		$maxAge = $this->GetVariable('maxAge');

		Validator::ValidateNotEmpty($title, 'A lesson title must be entered.', 'title');
		Validator::ValidateNotEmpty($type, 'A lesson type must be entered.', 'type');
		Validator::ValidateNotEmpty($url, 'A lesson URL must be entered.', 'url');

		if (strtolower($type) != 'pdf' && strtolower($type) != 'video')
			throw new APIException('The lesson type must be pdf or video.');

		$this->_object->title = $title;
		$this->_object->type = $type;
		$this->_object->minAge = $minAge;
		$this->_object->maxAge = $maxAge;
		$this->_object->updated = time();
		$this->_object->Update();
	}

	public function DownloadGET()
	{
		$this->RequireAuthentication();

		$download = new LessonDownload();
		$download->lesson_id = $this->_object->id;
		$download->user_id = $this->_user->id;
		$download->stamp = time();
		$download->Insert();
	}
}
