<?php 
error_reporting(E_ALL);
require_once __DIR__ . '/../vendor/autoload.php'; // Autoload files using Composer autoload

use Qik\Core\APIServer;
use Qik\Debug\Debugger;
use Qik\Utility\Utility;
use Qik\Database\{DBManager, DBQuery};

use Unicef\Controllers\{UserController, ResourceController, LessonController, LessonRatingController};

$server = new APIServer();
$server->RegisterDeveloper('172.19.0.1', 'Mike Stevens');
$server->RegisterController(new UserController);
$server->RegisterController(new ResourceController);
$server->RegisterController(new LessonController);
$server->RegisterController(new LessonRatingController);

$server->RegisterPostCache(function() {
	$connection = DBManager::CreateConnection('mysql', 'local', 'local', 'local');
	DBQuery::Connect($connection, true);
});

Debugger::SetTimestamp('init');

$server->Configure();
$server->Serve();

Debugger::SetTimestamp('denit');
?>